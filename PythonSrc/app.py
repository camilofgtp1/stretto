import os
import io
import glob
import numpy as np
import _sqlite3 as sqlite3
from _sqlite3 import Error
from PythonSrc import hdf5_getters


def get_cols_artist(file):

    if file.endswith(".h5"):
        file = hdf5_getters.open_h5_file_read(file)

        columns = {
            'id': hdf5_getters.get_artist_id(file),
            'similar_artists': hdf5_getters.get_similar_artists(file),
            'artist_hotness': hdf5_getters.get_artist_hotttnesss(file),
            'artist_longitude': hdf5_getters.get_artist_latitude(file),
        }

        file.close()
        return columns




def count_all_files(basedir, ext='.h5'):
    cnt = 0
    for root, dirs, files in os.walk(basedir):
        files = glob.glob(os.path.join(root, '*' + ext))
        cnt += len(files)
    return cnt


def get_all_tmp(basedir, ext='.h5'):
    for root, dirs, files in os.walk(basedir):
        files = glob.glob(os.path.join(root, '*' + ext))
        for f in files:
            h5 = hdf5_getters.open_h5_file_read(f)
            print hdf5_getters.get_segments_timbre(h5)
            h5.close()


def get_all_titles(basedir, ext='.h5'):
    titles = []
    for root, dirs, files in os.walk(basedir):
        files = glob.glob(os.path.join(root, '*' + ext))
        for f in files:
            h5 = hdf5_getters.open_h5_file_read(f)
            titles.append(hdf5_getters.get_title(h5))
            print hdf5_getters.get_title(h5)
            h5.close()
    return titles


def adapt_array(arr):
    """
    http://stackoverflow.com/a/31312102/190597 (SoulNibbler)
    """
    out = io.BytesIO()
    np.save(out, arr)
    out.seek(0)
    return sqlite3.Binary(out.read())

def flatten_array(arr):
    flat=""
    for i in arr:
        flat+=i+','

    return flat


def convert_array(text):
    out = io.BytesIO(text)
    out.seek(0)
    return np.load(out)


def get_cols_song(file):

    if file.endswith(".h5"):
        file = hdf5_getters.open_h5_file_read(file)

        columns = {
            'id': hdf5_getters.get_song_id(file),
            'title': hdf5_getters.get_title(file),
            'hotness': hdf5_getters.get_song_hotttnesss(file),
            'release': hdf5_getters.get_release(file),
            'similar_artists': np.array(hdf5_getters.get_similar_artists(file)),
            'duration': hdf5_getters.get_duration(file),
            'artist_id': hdf5_getters.get_artist_id(file),
            'segments_timbre': hdf5_getters.get_segments_timbre(file),
            'beats_start': np.array(hdf5_getters.get_beats_start(file)),
            'filename': str(file)
        }

        file.close()
        return columns

def insert_songs(columns):
    conn = None
    sql = ''' INSERT INTO song(id, title, hotness, release, similar_artists,
                            duration, artist_id, filename) 
              VALUES(?,?,?,?,?,?,?,?,?,?)'''

    similar_artists = flatten_array(columns["similar_artists"])
    timbre_segments = flatten_array(columns["segments_timbre"])

    try:
        conn = sqlite3.connect('jdbc:sqlite:millionsongDb.db')
        conn.execute(sql,
                     (columns['id'], str(columns['title']), str(columns['hotness']), columns['release'],
                      buffer(similar_artists), columns['duration'], columns['artist_id'], str(columns['num_songs']), columns['artist_name'],
                      columns['filename']))
        conn.commit()

    except Error as e:
        print(e)
    finally:
        if conn:
            conn.close()


def insert_all(data_base_dir):
    ext = 'h5'
    print 'there are '
    print count_all_files(data_base_dir)
    print 'files\n'

    for root, dirs, files in os.walk(data_base_dir):
        files = glob.glob(os.path.join(root, '*' + ext))
        for file in files:
            insert_songs(get_cols_song(file))

def all_getters():
    return filter(lambda x: x[:3] == 'get', hdf5_getters.__dict__.keys())

def attributes_file():
    #get_all_tmp('../MillionSongSubset')
    for i, j in get_cols_artist('../data/TRAXLZU12903D05F95.h5').items():
        print i
        print' : '
        print j

#insert_songs(get_cols("../data/TRAXLZU12903D05F95.h5"))
#insert_all('../MillionSongSubset')

# count_all_files('../MillionSongSubset')
# get_all_titles('../MillionSongSubset')
for i in all_getters():
    print i
    print '\n'


attributes_file()


